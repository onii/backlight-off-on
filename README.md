# Backlight off/on shell script

This is a super simple script to temporarily turn off display backlight
until a key is pressed. Requires `xbacklight`. Would work great for an alias.
