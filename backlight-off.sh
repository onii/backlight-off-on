#!/bin/bash
# Requires xbacklight
currLevel=`xbacklight -get`
echo 'Turning backlight off.'
sleep 1
xbacklight -set 0
date +"[""%D""]"\ %r
read -n 1 -s
echo 'Turning backlight on.'
date +"[""%D""]"\ %r
if [[ ${currLevel} < 5 ]]; then
    xbacklight -set 5
else
    xbacklight -set ${currLevel}
fi
